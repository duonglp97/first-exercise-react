import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserOutput from './UserOutput/UserOutput'
import UserInput from './UserInput/UserInput'

class App extends Component {
  state = {
    userName: 'duong'
  }



  changeStateHandle = (event) => {
    this.setState({
      userName: event.target.value
    })
  }

  render() {

    return (
      <div className="App">
        <br/>
        <UserInput changeState={this.changeStateHandle} name={this.state.userName}></UserInput>
        <UserOutput  name={this.state.userName}></UserOutput>
      </div>
    );
  }
}

export default App;
