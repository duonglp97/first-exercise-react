import React from 'react';

const userInput = (props) => {
    const style = {
        backgroundColor: 'white',
        font: 'inherit',
        boder: '1px solid blue',
        padding: '8px',
        width: '60%',
    }
    return (
        <div>
            <input style={style} onChange={props.changeState} value={props.name} type="text"/>
        </div>
    )
}

export default userInput;
